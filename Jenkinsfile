env.AWS_DEFAULT_REGION = 'ap-northeast-2'
env.SERVICE_NAME = 'jenkins-workshop-go-app'
env.ECR_IMAGE_TAG = null
env.PACKAGE_VERSION = '0.0.1'

properties([[$class: 'GitLabConnectionProperty', gitLabConnection: 'gitlab-connection']])

podTemplate(serviceAccount: 'jenkins', nodeSelector: 'jenkins=slave',
    yaml:"""
apiVersion: v1
kind: Pod
spec:
  tolerations:
  - key: jenkins
    operator: Equal
    value: slave
    effect: NoSchedule
    """,
    envVars: [
        envVar(key: 'AWS_DEFAULT_REGION', value: AWS_DEFAULT_REGION),
        envVar(key: 'DOCKER_BUILDKIT', value: '1')
    ],
    containers: [
        containerTemplate(name: 'go', image: 'golang:1.12-alpine', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'docker', image: 'docker:19.03.1-dind', ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'eks-helmsman', image: 'widerin/eks-helmsman:0.3', ttyEnabled: true, command: 'cat'),
    ],
    volumes: [
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
    ]
) {
    gitlabCommitStatus {
        node(POD_LABEL) {
            try {
                stage('Checkout') {
                    checkout scm
                }
                stage('Validate') {
                    if(!(env.PACKAGE_VERSION ==~ /\d+\.\d+\.\d+/)) {
                        error "Package version ${PACKAGE_VERSION} is not a valid version number. (Expect x.y.z)"
                    }
                }
                stage('Test') {
                    container('docker') {
                        sh "docker build -f Dockerfile-test ."
                    }
                }
                if (env.gitlabActionType == 'MERGE' || env.gitlabActionType == 'NOTE') {
                    stage('TestBuild') {
                        container('docker') {
                            sh "docker build ."
                        }
                    }
                }
                // Start Push Action
                if(env.gitlabActionType == 'PUSH') {
                    stage('Docker Image') {
                        container('eks-helmsman') {
                            env.ECR_IMAGE_URI = sh (script: "aws ecr describe-repositories --repository-names ${SERVICE_NAME} | grep repositoryUri | cut -d '\"' -s -f4", returnStdout: true).trim()
                            sh "aws ecr get-login --no-include-email | sed 's|https://||' > docker-ecr-login.sh"
                            sh "chmod +x docker-ecr-login.sh"
                        }
                        container('docker') {
                            sh "ash docker-ecr-login.sh"
                            sh "docker build -t ${ECR_IMAGE_URI}:${PACKAGE_VERSION} ."
                            sh "docker push ${ECR_IMAGE_URI}:${PACKAGE_VERSION}"
                        }
                    }
                    stage('Rollout') {
                        container('eks-helmsman') {
                            sh """
                            sed -i -e "s~{{IMAGE_WILL_BE_FILLED_DYNAMICALLY}}~${ECR_IMAGE_URI}~g" helm-chart/values.yaml
                            sed -i -e "s~{{TAG_WILL_BE_FILLED_DYNAMICALLY}}~${PACKAGE_VERSION}~g" helm-chart/values.yaml
                            helm upgrade --namespace default --install --recreate-pods ${SERVICE_NAME} ./helm-chart -f helm-chart/values.yaml
                            """
                        }
                    }
                }
            } catch (e) {
              currentBuild.result = "FAILED"
//              NotifyFailed()
              throw e
            }
        }
    }
}

def NotifyFailed() {
    emailext (
        subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: """FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'\r\nCheck console output at: \r\n"${env.BUILD_URL}\r\nPlease check out attachment for more log details""",
        attachLog: true,
        to: "${env.gitlabUserEmail}"
    )
}
